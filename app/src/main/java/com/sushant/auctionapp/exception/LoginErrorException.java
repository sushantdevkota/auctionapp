package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class LoginErrorException extends Exception {
    public LoginErrorException(String detailMessage) {
        super(detailMessage);
    }
}
