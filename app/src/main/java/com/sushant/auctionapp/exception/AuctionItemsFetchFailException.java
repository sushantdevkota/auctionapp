package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class AuctionItemsFetchFailException extends Exception {
    public AuctionItemsFetchFailException(String detailMessage) {
        super(detailMessage);
    }
}
