package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class ErrorMessageFactory {

    public static String createMessage(Exception e){
        return e.getMessage();
    }
}
