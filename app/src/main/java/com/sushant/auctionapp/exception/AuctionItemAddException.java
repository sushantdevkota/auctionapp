package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class AuctionItemAddException extends Exception{
    public AuctionItemAddException(String detailMessage) {
        super(detailMessage);
    }
}
