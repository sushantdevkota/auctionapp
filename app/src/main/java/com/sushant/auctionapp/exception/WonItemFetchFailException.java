package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class WonItemFetchFailException extends Exception{

    public WonItemFetchFailException(String detailMessage) {
        super(detailMessage);
    }
}
