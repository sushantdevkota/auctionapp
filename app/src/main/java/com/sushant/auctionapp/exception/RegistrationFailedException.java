package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class RegistrationFailedException extends Exception {
    public RegistrationFailedException(String detailMessage) {
        super(detailMessage);
    }
}
