package com.sushant.auctionapp.exception;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class BidAddException extends Exception{

    public BidAddException(String detailMessage) {
        super(detailMessage);
    }
}
