package com.sushant.auctionapp.wonitems;

import com.sushant.auctionapp.data.AuctionItem;
import com.sushant.auctionapp.data.Repository;
import com.sushant.auctionapp.exception.ErrorMessageFactory;
import com.sushant.auctionapp.exception.WonItemFetchFailException;

import java.util.List;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class WonItemsPresenter implements WonItemsContract.InteractionListener {

    private WonItemsContract.View view;
    private Repository repository;

    public WonItemsPresenter(WonItemsContract.View view, Repository repository){

        this.view = view;
        this.repository = repository;
    }


    @Override
    public void fetchWonItemList() {
        repository.fetchWonItemList(new Repository.FetchWonItemsCallback(){

            @Override
            public void onWonItemSuccessfullyFetched(List<AuctionItem> wonItemList) {
                view.onWonItemListFetched(wonItemList);
            }

            @Override
            public void onWonItemFetchFail(WonItemFetchFailException e) {
                view.onWonItemsFetchFail(ErrorMessageFactory.createMessage(e));
            }
        });
    }
}
