package com.sushant.auctionapp.auctionitems;

import com.sushant.auctionapp.data.AuctionItem;

import java.util.List;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public interface AuctionItemsContract {

    interface View{

        void onAuctionItemsFetched(List<AuctionItem> auctionItemList);

        void onAuctionItemsFetchFail(String message);
    }

    interface InteractionListener{
        void fetchAuctionItemsList();
    }
}
