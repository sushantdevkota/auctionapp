package com.sushant.auctionapp.register;

import com.sushant.auctionapp.data.User;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public interface RegisterContract {
    interface View{
        void showViewOnUserNameEmpty();

        void showViewOnPasswordEmpty();

        void showViewOnFullNameEmpty();

        void showRegistrationErrorView(String message);

        void showRegistrationSuccessView(User user);
    }

    interface InteractionListener{
        void register(User user, char[] password);

    }
}
