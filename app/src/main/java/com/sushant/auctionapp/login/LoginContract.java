package com.sushant.auctionapp.login;

import com.sushant.auctionapp.data.User;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public interface LoginContract {

    interface View{
        void showLoginError(String message);

        void showViewOnLoginSuccessFul(User user);

        void showUserNameEmptyErrorView();

        void showPasswordEmptyErrorView();

        void onUserLoggedIn(User user);

        void onUserNotLoggedIn();
    }

    interface InteractionListener{
        void login(String userName, char[] password);

        void checkIfSessionRunning();
    }
}
