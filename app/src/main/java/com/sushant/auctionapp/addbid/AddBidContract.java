package com.sushant.auctionapp.addbid;

import com.sushant.auctionapp.data.AuctionItem;
import com.sushant.auctionapp.data.Bid;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public interface AddBidContract {

    interface View{

        void showNotAValidNumberView();

        void showBidAddError(String message);

        void showBidSuccessfullyAddedView(Bid bid);
    }

    interface InteractionListener{
         void bid(AuctionItem auctionItem, String bidAmount);
    }
}
