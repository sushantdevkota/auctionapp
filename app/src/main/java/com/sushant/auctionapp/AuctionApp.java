package com.sushant.auctionapp;

import android.app.Application;

import com.sushant.auctionapp.data.DAO;

/**
 * Created by sushantdevkota on 5/17/16.
 */
public class AuctionApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DAO.instantiateDAO(getApplicationContext());
    }
}
